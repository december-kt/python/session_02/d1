# [Section] Control Structures

# [Section] User Input
# username = input("Please enter your username: \n")
# print(f"Hello {username}! Welcome to Python Short Course")


# [Section] Input Type Conversion 
# num1 = int(input("Enter the first number: "))
# num2 = int(input("Enter the second number: "))
# print(f"The sum of {num1} and {num2} is {num1+num2}")
 
# [Section] if-else Statements
# test_num = 75

# if test_num >= 69:
	# print("Test Passed")
# else:
 	# print("Test Failed")

# [Section] if-else Chains
# test_num2 = int(input("Please enter the second test number: "))

# if test_num2 > 0:
    # print("The number is positive")
# elif test_num2 == 0:
    # print("The number is zero")
# else: 
    # print("The number is negative")

# [Section] Mini Activity
# test_div_num = int(input("Please enter a number to test: "))

# if test_div_num % 3 == 0 and test_div_num % 5 == 0:
    # print(f"{test_div_num} is divisible by both 3 and 5")
# elif test_div_num % 3 == 0:
    # print(f"{test_div_num} is divisible by 3")
# elif test_div_num % 5 == 0:
    # print(f"{test_div_num} is divisible by 5")
# else:
    # print(f"{test_div_num} is not divisible by both 3 nor 5")

# [Section] Loops

# [Section] While Loops
# i = 1
# while i <= 5:
# 	print(f"Current Value: {i}")
# 	i += 1

# [Section] For Loops
# iterating over/through a sequence
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)
# # using range(start, stop, skip)
# for x in range(6):
# 	print(f"The current value is {x}")
# for y in range(6, 10):
# 	print(f"The current value is {y}")
# for z in range(6, 20, 2):
# 	print(f"The current value is {z}")

# [Section] Break Statement
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j += 1

# [Section] Continue Statement
k = 1
while k < 6:
	# increment should be at the top of the code
	k += 1
	if k == 3:
		continue
	print(k) 

